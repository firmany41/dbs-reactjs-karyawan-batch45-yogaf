import React, { useContext, useEffect } from "react";

import TableProduct from "../components/TableProduct";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";

function TablePage({}) {
  const { fetchProducts } = useContext(ProductContext);
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Table Product</title>
      </Helmet>
      <Layout>
        <TableProduct />
      </Layout>
    </div>
  );
}

export default TablePage;
