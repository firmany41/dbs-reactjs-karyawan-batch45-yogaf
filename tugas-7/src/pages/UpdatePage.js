import React from "react";
import UpdateForm from "../components/UpdateForm";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";

function UpdatePage({}) {
  return (
    <div>
      <Helmet>
        <title>Update Product</title>
      </Helmet>
      <Layout>
        <UpdateForm />
      </Layout>
    </div>
  );
}

export default UpdatePage;
