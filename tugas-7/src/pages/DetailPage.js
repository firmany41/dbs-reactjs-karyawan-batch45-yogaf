import React, { useContext, useEffect } from "react";
import DetailProduct from "../components/DetailProduct";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";

function DetailPage({}) {
  return (
    <section className="bg-gray-50">
      <Helmet>
        <title>Detail Product</title>
      </Helmet>
      <Layout>
        <DetailProduct />
      </Layout>
    </section>
  );
}

export default DetailPage;
