import React from "react";
import Form from "../components/Form";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";

function CreatePage({}) {
  return (
    <div className="bg-gray-100">
      <Helmet>
        <title>Create Product</title>
      </Helmet>
      <Layout>
        <Form />
      </Layout>
    </div>
  );
}

export default CreatePage;
