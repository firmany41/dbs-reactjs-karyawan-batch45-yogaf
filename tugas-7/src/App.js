import HomePage from "./pages/HomePage";
import CreatePage from "./pages/CreatePage";
import UpdatePage from "./pages/UpdatePage";
import TablePage from "./pages/TablePage";
import DetailPage from "./pages/DetailPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ProductProvider } from "./context/ProductContext";
import './App.css';

function App() {
  return (
    <div>
      <BrowserRouter>
        <ProductProvider>
          <Routes>
            <Route path="/" element={<HomePage />}></Route>
            <Route path="/create" element={<CreatePage />}></Route>
            <Route path="/update/:id" element={<UpdatePage />}></Route>
            <Route path="/table" element={<TablePage />}></Route>
            <Route path="/product/:id" element={<DetailPage />}></Route>
          </Routes>
        </ProductProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
