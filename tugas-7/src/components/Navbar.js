import React from "react";
import logo from "../img/logo.svg";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div>
     <header class="shadow-lg py-4 bg-white px-12 sticky top-0">
        <nav class="mx-auto max-w-7xl flex justify-between">
          <div>
            <a href="/">
              <img src={logo} className="w-32" alt="Logo Trialshop" />
            </a>
          </div>
          <ul class="flex items-center gap-4 text-cyan-500 text-xl">
            <Link to="/">
              <li>Home</li>
            </Link>
            <Link to="/table">
              <li>Table</li>
            </Link>
          </ul>
          <div class="flex items-center gap-4 w-32"></div>
        </nav>
      </header>
    </div>
  );
}

export default Navbar;
