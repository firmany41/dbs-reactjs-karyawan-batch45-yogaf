import axios from "axios";
import { useEffect, useState } from "react";
import Form from "./components/Form";
import Navbar from "./components/Navbar";
import Card from "./components/Card";

function App() {
  const [products, setProducts] = useState([]);
  const fetchProducts = async () => {
    try {
      // fetch data
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/products"
      );
      setProducts(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    console.log("Fetch Data");
    fetchProducts();
  }, []);

  return (
    <div>
      <Navbar />

      <section class="mx-32">
        <Form fetchProducts={fetchProducts} />
        <h1 className="my-8 text-3xl font-bold justify-start">
          Catalog Products
        </h1>
        <div className="grid grid-cols-4 gap-8">
          {products.map((item, index) => {
            return <Card product={item} />;
          })}
        </div>
      </section>
    </div>
  );
}

export default App;
