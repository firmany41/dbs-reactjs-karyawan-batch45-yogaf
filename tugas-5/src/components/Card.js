import React from "react";

function Card({ product }) {
  return (
    <div>
      <a href="#">
        <div class="shadow-lg">
          <img
            src={product.image_url}
            class="w-full h-72 object-cover"
            alt="Product Image"
          />
          <div class="pl-4 my-4">
            <div className="grid grid-row-3 gap-2 py-2 px-2">
              <h1 className="w-50 h-10">{product.name}</h1>
              <p className="line-through text-red-500">{product.harga_display}</p>
              <p className="">{product.harga_diskon_display}</p>
              <p className="text-blue-400">Stock : {product.stock}</p>
            </div>
          </div>
        </div>
      </a>
    </div>
  );
}

export default Card;
