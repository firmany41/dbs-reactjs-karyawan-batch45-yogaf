import React from "react";
import logo from "../img/logo.svg";

function Navbar() {
  return (
    <div>
      <header className="flex justify-around items-center bg-white py-3 shadow-lg">
      <div>
      <a href="#"><img src={logo} className="w-32" alt="Logo Trialshop" /></a>
      </div>
        <div className="flex gap-16">
          <a href="./home.html">Home</a>
          <a href="./form.html">Form</a>
          <a href="./product.html">Products</a>
        </div>
        <div>
          <button className="bg-gray-200 px-4 py-2">Login</button>
          <button className="bg-gray-200 px-4 py-2">Register</button>
        </div>
      </header>
    </div>
  );
}

export default Navbar;
