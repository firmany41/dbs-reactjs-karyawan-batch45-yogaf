import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import DetailProduct from "../components/DetailProduct";
import { ProductContext } from "../context/ProductContext";

function DetailPage({}) {
  return (
    <section className="bg-gray-50">
      <Navbar />
      <DetailProduct />
    </section>
  );
}

export default DetailPage;
