import React from "react";
import Navbar from "../components/Navbar";
import Form from "../components/Form";

function CreatePage({ }) {
  return (
    <div className="bg-gray-50">
      <Navbar />
      <Form />
    </div>
  );

}

export default CreatePage;
