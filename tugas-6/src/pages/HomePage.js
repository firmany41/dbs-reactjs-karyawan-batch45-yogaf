import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { ProductContext } from "../context/ProductContext";

function HomePage({}) {
  const { products, fetchProducts, loading, status } =
    useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <section className="bg-gray-50">
      <Navbar />

      <section className="mx-32">
        <h1 className="my-8 text-3xl font-bold justify-start">
          Catalog Products
        </h1>
        {loading === false ? (
          <div className="grid grid-cols-4 gap-8">
            {products.map((item, index) => {
              return <Product product={item} />;
            })}
          </div>
        ) : (
          <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
        )}
      </section>
    </section>
  );
}

export default HomePage;
