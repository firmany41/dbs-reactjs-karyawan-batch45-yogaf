import axios from "axios";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";

function Form({}) {
  const { fetchProducts } = useContext(ProductContext);
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "",
    harga: 0,
    stock: 0,
    image_url: "",
    is_diskon: false,
    harga_diskon: 0,
    category: "",
    description: "",
  });

  const handleChange = (e) => {
    if (e.target.name === "name") {
      setInput({ ...input, name: e.target.value });
    } else if (e.target.name === "harga") {
      setInput({ ...input, harga: e.target.value });
    } else if (e.target.name === "stock") {
      setInput({ ...input, stock: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    } else if (e.target.name === "is_diskon") {
      setInput({ ...input, is_diskon: e.target.checked });
    } else if (e.target.name === "harga_diskon") {
      setInput({ ...input, harga_diskon: e.target.value });
    } else if (e.target.name === "category") {
      setInput({ ...input, category: e.target.value });
    } else if (e.target.name === "description") {
      setInput({ ...input, description: e.target.value });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(input);
  };

  const resetForm = () => {
    setInput({
      name: "",
      harga: "",
      stock: "",
      image_url: "",
      is_diskon: false,
      harga_diskon: "",
      category: "",
      description: "",
    });
  };

  const storeProduct = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        {
          name: input.name,
          harga: input.harga,
          stock: input.stock,
          image_url: input.image_url,
          is_diskon: input.is_diskon,
          harga_diskon: input.harga_diskon,
          category: input.category,
          description: input.description,
        }
      );
      alert("Berhasil Mengirim Request");
      fetchProducts();
      resetForm();
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  return (
    <div class="mt-24 container pb-16 mx-auto bg-white shadow-xl">
      <section>
        <h1 className="text-center text-4xl">Form Product</h1>
        <form>
          <div class="grid grid-cols-2 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Nama :
              </label>
              <input
                type="text"
                placeholder="Masukkan Nama Barang"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="name"
              />
            </div>
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Stock :
              </label>
              <input
                type="number"
                placeholder="Masukkan Stock"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="stock"
              />
            </div>
          </div>
          <div class="grid grid-cols-3 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Harga :
              </label>
              <input
                type="number"
                placeholder="Masukkan Harga"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="harga"
              />
            </div>
            <div className="flex items-center mt-5 justify-center">
              <label
                htmlFor=""
                className="mr-2 text-sm font-medium text-black-900 dark:text-black-300"
              >
                Status Diskon :
              </label>
              <input
                type="checkbox"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                onChange={handleChange}
                name="is_diskon"
              />
            </div>
            {input.is_diskon ? (
              <div className="my-4">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Harga Diskon :
                </label>
                <input
                  type="number"
                  placeholder="Masukkan Harga"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  name="harga_diskon"
                />
              </div>
            ) : (
              <div></div>
            )}
          </div>
          <div class="grid grid-cols-2 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Image URL:
              </label>
              <input
                type="url"
                placeholder="Masukkan image URL"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="image_url"
              />
            </div>
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Category :
              </label>
              <select
                onChange={handleChange}
                name="category"
                id=""
                class="bg-white-50 border border-gray-300  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
              >
                <option value="" selected disabled>
                  Pilih Kategori Barang
                </option>
                <option value="teknologi">Teknologi</option>
                <option value="makanan">Makanan</option>
                <option value="minuman">Minuman</option>
                <option value="hiburan">Hiburan</option>
                <option value="kendaraan">Kendaraan</option>
              </select>
            </div>
          </div>
          <div className="grid py-3 px-3">
            <label
              htmlFor=""
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
            >
              Description :
            </label>
            <input
              type="text"
              placeholder="Masukkan Deskripsi Barang"
              className="block p-2.5 w-full text-sm text-gray-900 bg-white-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
              onChange={handleChange}
              name="description"
            />
          </div>
          <div class="justify-end flex md:order-2 gap-2 px-3 py-3">
            <button
              type="button"
              onClick={storeProduct}
              className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
            >
              Submit
            </button>
          </div>
        </form>
      </section>
    </div>
  );
}

export default Form;
