import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";

function TableProduct() {
  const { products, fetchProducts, moveToCreate } = useContext(ProductContext);

  const onUpdate = (product) => {};
  const onDelete = async (id) => {
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      alert("Berhasil Menghapus Product");
      fetchProducts();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center ">Table Product</h1>
      <div className="max-w-4xl mx-auto w-full my-4 ">
        <div class="justify-end flex px-3 ">
        <button
          onClick={moveToCreate}
          className="flex px-6 py-2 bg-blue-500 text-white my-4 rounded-lg justify-end"
        >
          Create Product +
        </button>
        </div>
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Nama</th>
              <th className="border border-gray-500 p-2">Status Diskon</th>
              <th className="border border-gray-500 p-10">Harga</th>
              <th className="border border-gray-500 p-10">Harga Diskon</th>
              <th className="border border-gray-500 p-2">Gambar</th>
              <th className="border border-gray-500 p-2">Kategori</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {products.map((item, index) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{item.id}</td>
                  <td className="border border-gray-500 p-2">{item.name}</td>
                  <td className="border border-gray-500 p-2">
                    {item.is_diskon === true ? "Aktif" : "Mati"}
                  </td>
                  <td className="border border-gray-500 p-2">{item.harga_display}</td>
                  <td className="border border-gray-500 p-2">
                    {item.harga_diskon_display}
                  </td>
                  <td className="border border-gray-500 p-2">
                    <img src={item.image_url} alt="" className="w-full h-32 object-cover rounded-xl" />
                  </td>
                  <td className="border border-gray-500 p-2">
                    {item.category}
                  </td>
                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <Link to={`/update/${item.id}`}>
                        <button className="px-3 py-1 bg-yellow-600 text-white">
                          Update
                        </button>
                      </Link>
                      <button
                        onClick={() => onDelete(item.id)}
                        className="px-3 py-1 bg-red-600 text-white"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default TableProduct;
