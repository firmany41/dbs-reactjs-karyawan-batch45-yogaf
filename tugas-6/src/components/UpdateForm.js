import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

function UpdateForm() {
  const { id } = useParams();
  const navigate = useNavigate();

  const [input, setInput] = useState({
    name: "",
    harga: 0,
    stock: 0,
    image_url: "",
    is_diskon: false,
    harga_diskon: 0,
    category: "",
    description: "",
  });

  const handleChange = (e) => {
    if (e.target.name === "name") {
      setInput({ ...input, name: e.target.value });
    } else if (e.target.name === "harga") {
      setInput({ ...input, harga: e.target.value });
    } else if (e.target.name === "stock") {
      setInput({ ...input, stock: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    } else if (e.target.name === "is_diskon") {
      setInput({ ...input, is_diskon: e.target.checked });
    } else if (e.target.name === "harga_diskon") {
      setInput({ ...input, harga_diskon: e.target.value });
    } else if (e.target.name === "category") {
      setInput({ ...input, category: e.target.value });
    } else if (e.target.name === "description") {
      setInput({ ...input, description: e.target.value });
    }
  };

  const resetForm = () => {
    setInput({
      name: "",
      harga: "",
      stock: "",
      image_url: "",
      is_diskon: false,
      harga_diskon: "",
      category: "",
      description: "",
    });
  };

  const fetchProductById = async () => {
    try {
      // fetch data
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      console.log(response.data.data);
      const product = response.data.data;
      // melakukan binding data dari server
      setInput({
        name: product.name,
        harga: product.harga,
        stock: product.stock,
        image_url: product.image_url,
        is_diskon: product.is_diskon,
        harga_diskon: product.harga_diskon,
        category: product.category,
        description: product.description,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const updateProduct = async () => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/products/${id}`,
        {
          name: input.name,
          harga: input.harga,
          stock: input.stock,
          image_url: input.image_url,
          is_diskon: input.is_diskon,
          harga_diskon: input.harga_diskon,
          category: input.category,
          description: input.description,
        }
      );
      alert("Berhasil mengupdate product");
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  useEffect(() => {
    console.log(`Fetch Product id ${id}`);
    fetchProductById();
  }, []);

  return (
    <div class="mt-24 container pb-16 mx-auto bg-white shadow-xl">
      <section>
        <h1 className="text-center text-4xl">
          Form Update Product dengan ID {id}
        </h1>
        <form>
          <div class="grid grid-cols-2 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Nama :
              </label>
              <input
                type="text"
                placeholder="Masukkan Nama Barang"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="name"
                value={input.name}
              />
            </div>
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Stock :
              </label>
              <input
                type="number"
                placeholder="Masukkan Stock"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="stock"
                value={input.stock}
              />
            </div>
          </div>
          <div class="grid grid-cols-3 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Harga :
              </label>
              <input
                type="number"
                placeholder="Masukkan Harga"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="harga"
                value={input.harga}
              />
            </div>
            <div className="flex items-center mt-5 justify-center">
              <label
                htmlFor=""
                className="mr-2 text-sm font-medium text-black-900 dark:text-black-300"
              >
                Status Diskon :
              </label>
              <input
                type="checkbox"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                onChange={handleChange}
                name="is_diskon"
                checked={input.is_diskon}
              />
            </div>
            {input.is_diskon ? (
              <div className="my-4">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Harga Diskon :
                </label>
                <input
                  type="number"
                  placeholder="Masukkan Harga"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  name="harga_diskon"
                  value={input.harga_diskon}
                />
              </div>
            ) : (
              <div></div>
            )}
          </div>
          <div class="grid grid-cols-2 gap-6 py-3 px-3">
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Image URL:
              </label>
              <input
                type="url"
                placeholder="Masukkan image URL"
                className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                name="image_url"
                value={input.image_url}
              />
            </div>
            <div className="my-4">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Category :
              </label>
              <select
                onChange={handleChange}
                name="category"
                id="category"
                value={input.category}
                class="bg-white-50 border border-gray-300  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
              >
                <option value="" disabled>
                  Pilih Kategori Barang
                </option>
                <option value="teknologi">Teknologi</option>
                <option value="makanan">Makanan</option>
                <option value="minuman">Minuman</option>
                <option value="hiburan">Hiburan</option>
                <option value="kendaraan">Kendaraan</option>
              </select>
            </div>
          </div>
          <div className="grid py-3 px-3">
            <label
              htmlFor=""
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
            >
              Description :
            </label>
            <input
              type="text"
              placeholder="Masukkan Deskripsi Barang"
              className="block p-2.5 w-full text-sm text-gray-900 bg-white-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
              onChange={handleChange}
              name="description"
              value={input.description}
            />
          </div>
          <div class="justify-end flex md:order-2 gap-2 px-3 py-3">
            <button
              type="button"
              onClick={updateProduct}
              className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
            >
              Update
            </button>
          </div>
        </form>
      </section>
    </div>
  );
}

export default UpdateForm;
